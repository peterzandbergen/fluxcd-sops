#!/bin/bash

flux bootstrap gitlab \
    --ssh-hostname=gitlab.com \
    --owner=peterzandbergen \
    --repository=fluxcd-sops \
    --branch=master \
    --path=clusters/test-cluster \
    $*

